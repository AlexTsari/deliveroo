import { Stack, useNavigation } from 'expo-router';
import { useColorScheme } from 'react-native';
import CustomHeader from '../Components/CustomHeader';
import { BottomSheetModalProvider } from '@gorhom/bottom-sheet';
import { Ionicons } from '@expo/vector-icons'
import Colors from '../constants/Colors';
import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
} from 'react-native'
import { ColorSpace } from 'react-native-reanimated';
// import Filter from './(modal)/filter'
export const unstable_settings = {
  // Ensure that reloading on `/modal` keeps a back button present.
  initialRouteName: 'index',
};

export default function RootLayoutNav() {
  const navigation = useNavigation();

  return (
    <BottomSheetModalProvider>
      <Stack>
        <Stack.Screen
          name="index"
          options={{
            header: () => <CustomHeader />
          }} />
        <Stack.Screen
          name='(modal)/filter'
          options={{
            presentation: "modal",
            headerTitle: "Filter",
            headerShadowVisible: false,
            headerStyle: {
              backgroundColor: Colors.lightGray
            },
            headerLeft: () => (
              <TouchableOpacity onPress={() => { navigation.goBack() }}>
                <Ionicons name="close-outline" size={28} color={Colors.primary} />
              </TouchableOpacity >

            )
          }}
        />
        <Stack.Screen
          name='(modal)/location-search'
          options={{
            presentation: "fullScreenModal",
            headerTitle: "Select location",
            headerLeft: () => (
              <TouchableOpacity onPress={() => { navigation.goBack() }}>
                <Ionicons name="close-outline" size={28} color={Colors.primary} />
              </TouchableOpacity >

            )
          }}
        />
        <Stack.Screen
          name='(modal)/dish'
          options={{
            presentation: "modal",
            headerTitle: "",
            headerTransparent: true,
            // headerShadowVisible: false,
            headerLeft: () => (
              <TouchableOpacity
                style={{ backgroundColor: "#fff", borderRadius: 20, padding: 6 }}
                onPress={() => { navigation.goBack() }}>
                <Ionicons name="close-outline" size={28} color={Colors.primary} />
              </TouchableOpacity >
            )
          }}
        />
        <Stack.Screen
          name='basket'
          options={{
            headerTitle: "Basket",
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.goBack();
                }}
              >
                <Ionicons name='arrow-back' size={28} color={Colors.primary} />
              </TouchableOpacity>
            )
          }}
        />
      </Stack>
    </BottomSheetModalProvider>
  );
}
