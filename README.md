# Deliveroo Clone

Hi and welcome to this Deliveroo UI clone.

The project uses React native with dummy data and it is focused more on the UI aspect. In future I will definitely add some backend logic with authentication, order history, coupons and more. 

# Features
- Browse different restaurants.
- Search for your current location with Google maps API.
- Add and manage products in your cart.
- Complete order

# Main Technologies
- Expo Router
- Zustand
- Reanimated
- Google API

# Getting started
- git clone the repository
- download Expo Go on your mobile device
- npx expo in the root directory
- scan the QR code and wait for the app to build
- Enjoy the app on your mobile device

# Checkout some images from the app
## Main Page
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.09_5f4eb4a2.jpg>)
## First Modal
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.10_ac0b7367.jpg>)
## Find your location with Google API
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.10_98e05858.jpg>)
## Restaurant menu with item in the basket
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.12_5b0d8ea2.jpg>)
## Single product view
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.11_f139b9c5.jpg>)
## Filter Modal
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.09_76e197b5.jpg>)
## Swipe to delete a product in the basket
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.12_7639a0de.jpg>)
## Complete your order
![Alt text](<deliverooClone/assets/gitImages/WhatsApp Image 2023-11-09 at 21.14.13_4d366fa8.jpg>)