import {
    View,
    Text,
    Button,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'
import { BottomSheetModal, BottomSheetBackdrop, useBottomSheetModal } from '@gorhom/bottom-sheet'
import React, { forwardRef, useCallback, useMemo, } from 'react'
import Colors from '../constants/Colors';
import { Ionicons } from '@expo/vector-icons'
import { Link } from 'expo-router'

export type Ref = BottomSheetModal;

const BottomSheet = forwardRef<Ref>((props, ref) => {
    const snapPoints = useMemo(() => ["50%"], [])
    const renderBackdrop = useCallback((props: any) => <BottomSheetBackdrop appearsOnIndex={0} disappearsOnIndex={-1} {...props} />, []);
    const { dismiss } = useBottomSheetModal();
    return (
        <BottomSheetModal
            handleIndicatorStyle={{ display: "none" }}
            backgroundStyle={{ borderRadius: 0, backgroundColor: Colors.lightGray }}
            ref={ref}
            snapPoints={snapPoints}
            overDragResistanceFactor={0}
            backdropComponent={renderBackdrop}
        >
            <View style={styles.contentContainer}>
                <View style={styles.toggle}>
                    <TouchableOpacity style={styles.toggleActive}>
                        <Text style={styles.activeText}>
                            Delivery
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.toggleInactive}>
                        <Text style={styles.InactiveText}>
                            Pickup
                        </Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Text style={styles.subheader}>Your location</Text>
                    <Link href={"/(modal)/location-search"} asChild>
                        <TouchableOpacity>
                            <View style={styles.item}>
                                <Ionicons  name="location-outline" size={20} color={Colors.medium}/>
                                <Text style={{flex: 1}}>Current location</Text>
                                <Ionicons  name="chevron-forward" size={20} color={Colors.primary}/>
                            </View>
                        </TouchableOpacity>
                    </Link>
                    <Text style={styles.subheader}>Arrival Time</Text>
                    <Link href={"/"} asChild>
                        <TouchableOpacity>
                            <View style={styles.item}>
                                <Ionicons  name="stopwatch-outline" size={20} color={Colors.medium}/>
                                <Text style={{flex: 1}}>Now</Text>
                                <Ionicons  name="chevron-forward" size={20} color={Colors.primary}/>
                            </View>
                        </TouchableOpacity>
                    </Link>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => dismiss()}>
                    <Text style={styles.buttonText}>Confirm</Text>
                </TouchableOpacity>
            </View>
        </BottomSheetModal>
    )
});

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold'
    },
    button: {
        backgroundColor: Colors.primary,
        padding: 16,
        margin: 16,
        borderRadius: 4,
        alignItems: 'center'
    },
    toggle: {
        flexDirection: "row",
        justifyContent: "center",
        gap: 10,
        marginBottom: 32,
    },
    toggleActive: {
        backgroundColor: Colors.primary,
        padding: 8,
        borderRadius: 32,
        paddingHorizontal: 30,
    },
    toggleInactive: {
        // backgroundColor: Colors.pri,
        padding: 8,
        borderRadius: 32,
        paddingHorizontal: 30,
    },
    activeText: {
        color: '#fff',
        fontWeight: '700'
    },
    InactiveText: {
        color: Colors.primary,
        //fontWeight: '700'
    },
    subheader: {
        fontSize: 16,
        fontWeight: "600",
        margin: 16,
    },
    item: {
        flexDirection: "row",
        gap: 8,
        alignItems: "center",
        backgroundColor: '#fff',
        padding: 16,
        borderColor: Colors.grey,
        borderWidth: 1,
    }
})

export default BottomSheet